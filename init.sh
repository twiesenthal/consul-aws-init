#!/usr/bin/env bash
echo "test" >> /root/test.file

CONSUL=consul
CONSUL_VERSION=1.7.3
CONSUL_ARCHIV=${CONSUL}_${CONSUL_VERSION}_linux_amd64.zip
CONSUL_DIR=/opt/$CONSUL

VAULT=vault
VAULT_VERSION=1.4.1
VAULT_ARCHIV=${VAULT}_${VAULT_VERSION}_linux_amd64.zip
VAULT_DIR=/opt/$VAULT

NOMAD=nomad
NOMAD_VERSION=0.11.1
NOMAD_ARCHIV=${NOMAD}_${NOMAD_VERSION}_linux_amd64.zip
NOMAD_DIR=/opt/$NOMAD

# consul install
useradd -r ${CONSUL}
mkdir -p $CONSUL_DIR
cd $CONSUL_DIR
wget https://releases.hashicorp.com/${CONSUL}/${CONSUL_VERSION}/${CONSUL_ARCHIV}
unzip $CONSUL_ARCHIV
rm $CONSUL_ARCHIV
ln -s /opt/${CONSUL}/${CONSUL} /usr/bin/${CONSUL}
cp /consul-aws-init/services/${CONSUL}.service /lib/systemd/system/${CONSUL}.service

# vault install
useradd -r ${VAULT}
mkdir -p $VAULT_DIR
cd $VAULT_DIR
wget https://releases.hashicorp.com/${VAULT}/${VAULT_VERSION}/${VAULT_ARCHIV}
unzip $VAULT_ARCHIV
rm $VAULT_ARCHIV
ln -s /opt/${VAULT}/${VAULT} /usr/bin/${VAULT}
cp /consul-aws-init/services/${VAULT}.service /lib/systemd/system/${VAULT}.service

#vault config
mkdir -p /etc/vault
cp /consul-aws-init/etc/vault/config.hcl /etc/vault


# nomad install
useradd -r ${NOMAD}
mkdir -p $NOMAD_DIR
cd $NOMAD_DIR
wget https://releases.hashicorp.com/${NOMAD}/${NOMAD_VERSION}/${NOMAD_ARCHIV}
unzip $NOMAD_ARCHIV
rm $NOMAD_ARCHIV
ln -s /opt/${NOMAD}/${NOMAD} /usr/bin/${NOMAD}
cp /consul-aws-init/services/${NOMAD}.service /lib/systemd/system/${NOMAD}.service

systemctl daemon-reload

#rm -rf /consul-aws-init/
